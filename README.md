# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## General:
---------------

- Author: Toby Wong

- Email: chunyauw@uoregon.edu

- Description: This programme is a practice for understanding the relationship and functions between python and .ini configurations.